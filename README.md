Website for RSS 2016 Conference

Using [Bootstrap](http://getbootstrap.com/) framework for mobile-friendly design and [Grunt](http://gruntjs.com/) to automate integration of common elements such as navigation bars.

Installation Requirements:

`sudo apt-get install npm`

`sudo npm install -g grunt-cli`

On Ubuntu 14.04, you may need to define the following symlink to get grunt to run:

`ln -s /usr/bin/nodejs /usr/bin/node`

Inside the git repo, run:

`sudo npm install`

to get the required modules.

Compile with the `grunt` command. To auto-compile after a change, run `grunt watch`.