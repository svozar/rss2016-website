module.exports = function(grunt) {
  grunt.initConfig( {
    bake: {
      build: {
        files: [
      {
        expand: true,
        cwd: 'src/',
        src: '*.html',
        dest: 'build/',
        ext: '.html',
      }
      ]
    }
  },
  copy: {
    main: {
      files: [
        {expand: true, flatten: true, src: ['src/css/*'], dest: 'build/css/', filter: 'isFile'},
        {expand: true, flatten: true, src: ['src/img/*'], dest: 'build/img/', filter: 'isFile'},
        {expand: true, flatten: true, src: ['src/docs/*'], dest: 'build/docs/', filter: 'isFile'},
      ],
    },
  },
  prettify: {
    files: {
      expand: true,
      cwd: 'build/',
      ext: '.html',
      src: ['*.html'],
      dest: 'build/'
    }
  },
  watch: {
    bake: {
      files: [ "src/**" ],
      tasks: "bake:build"
    },
  }
} )
grunt.loadNpmTasks( "grunt-bake" );
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-copy');
grunt.loadNpmTasks('grunt-prettify');
grunt.registerTask('default', ['copy', 'bake', 'prettify']);
};
